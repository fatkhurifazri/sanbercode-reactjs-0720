// Tulislah sebuah function dengan nama halo() yang mengembalikan nilai “Halo Sanbers!” yang kemudian dapat ditampilkan di console.
console.log("Jawaban No.1")
function halo(){
    return("Halo Sanbers")
}
console.log(halo());

// Tulislah sebuah function dengan nama kalikan() yang mengembalikan hasil perkalian dua parameter yang di kirim.

function kalikan(num1,num2){
    return num1 * num2
}
var num1 = 12
var num2 = 4
var hasilKali = kalikan(num1, num2)
console.log(hasilKali); // 48

// Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: 
// “Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!”

function introduce() {
    perkenalan="Nama saya" + " " + name + ","  + " " +"Umur saya" + " " + age +" " + "tahun" + "," + " " + "alamat saya di" + " " + address + "," +" " + "dan saya punya hobby yaitu" +" " + hobby +"!"
    return perkenalan
}
 
var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!" 