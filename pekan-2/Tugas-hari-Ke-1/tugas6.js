// soal no 1
// ubahlah array di bawah ini menjadi object dengan property nama, jenis kelamin, hobi dan tahun lahir

// var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
console.log("Jawaban No.1")
var DaftarPeserta = {
    nama: "Asep" ,
    "jenis kelamin" : "laki-laki" ,
    hobi: "baca buku" ,
    "tahun lahir" : 1992 ,
}
console.log(DaftarPeserta)

// soal no.2
// anda diberikan data-data buah seperti di bawah ini

// 1.nama: strawberry
//   warna: merah
//   ada bijinya: tidak
//   harga: 9000 
// 2.nama: jeruk
//   warna: oranye
//   ada bijinya: ada
//   harga: 8000
// 3.nama: Semangka
//   warna: Hijau & Merah
//   ada bijinya: ada
//   harga: 10000
// 4.nama: Pisang
//   warna: Kuning
//   ada bijinya: tidak
//   harga: 5000

// uraikan data tersebut menjadi array of object dan munculkan data pertama
console.log("jawaban no.2")
var buah = [{nama : "strawberry", warna : "merah", "ada bijinya" : "tidak", harga : 9000},{nama : "jeruk", warna : "oranye", "ada bijinya" : "ada", harga : 8000}, {nama :"Semangka", warna : "Hijau & Merah", "ada bijinya" : "ada", harga : 10000},{nama :"Pisang", warna : "Kuning", "ada bijinya" : "tidak", harga :5000}]
console.log(buah[0]);

// soal No.3
// buatlah variable seperti di bawah ini

// var dataFilm = []

// buatlah fungsi untuk menambahkan dataFilm tersebut yang itemnya object adalah object dengan property nama, durasi , genre, tahun
console.log("Jawaban no.3")
var dataFilm=[]
    function tambahDataFilm(ironMan){
        return dataFilm.push(ironMan)
    }
    var ironMan = {
        nama : "Iron man",
        durasi : 120,
        genre :  "Super Heroes",
        tahun : 2008,
    }
    var baru = tambahDataFilm(ironMan)
    console.log(dataFilm);  
    
// Terdapat sebuah class Animal yang memiliki sebuah constructor name, default property band= 4 dan cold_blooded = false.

// Soal No.4
// Release 0

// Buatlah class Animal yang menerima satu parameter constructor berupa name. Secara default class Animal akan memiliki property yaitu legs (jumlah kaki) yang bernilai 4 dan cold_blooded bernilai false.

//     Gunakan method getter untuk mengakses property di dalam class

// class Animal {
//     // Code class di sini
// }
 
// var sheep = new Animal("shaun");
 
// console.log(sheep.name) // "shaun"
// console.log(sheep.legs) // 4
// console.log(sheep.cold_blooded) // false

// Release 1

// Buatlah class Frog dan class Ape yang merupakan inheritance dari class Animal. Perhatikan bahwa Ape (Kera) merupakan hewan berkaki 2, hingga dia tidak menurunkan sifat jumlah kaki 4 dari class Animal. class Ape memiliki function yell() yang menampilkan “Auooo” dan class Frog memiliki function jump() yang akan menampilkan “hop hop”.

// // Code class Ape dan class Frog di sini
 
// var sungokong = new Ape("kera sakti")
// sungokong.yell() // "Auooo"
 
// var kodok = new Frog("buduk")
// kodok.jump() // "hop hop"
console.log("Jawaban no.4") 
// release 0
class Animal{
    constructor (name, legs, cold_blooded){
        this.name = name ;
        this.legs = 4 ; 
        this.cold_blooded = false;
    }
    get name1 (){
        return this.name;
    }
    get legs1 (){
        return this.legs;
    }
    get cold_blooded1 (){
        return this.cold_blooded;
    }
}
    var sheep = new Animal ("shaun");

    console.log(sheep.name1) // "shaun"
    console.log(sheep.legs1) // 4
    console.log(sheep.cold_blooded1) // false
// release 1
class Ape extends Animal{
    constructor(name){
        super(name)
        this.legs= 2
    }
    yell() { //<<<ini adalah method
        return "Auooo";
    }
}
class Frog extends Animal{
    constructor(name){
        super(name)
        this.legs= 4
    }
    jump() { //<<<ini adalah method
        return "hop hop"
    }
}
    var sungokong = new Ape("kera sakti")
    console.log (sungokong.yell()) // "Auooo"

    var kodok = new Frog("buduk")
    console.log (kodok.jump()) // "hop hop" 


// Soal No.5
//     Terdapat sebuah class dengan nama Clock yang ditulis seperti penulisan pada function, ubahlah fungsi tersebut menjadi class dan pastikan fungsi tersebut tetap berjalan dengan baik. Jalankan fungsi di terminal/console Anda untuk melihat hasilnya. (tekan tombol Ctrl + C pada terminal untuk menghentikan method clock.start())

//     Hint: Fokus soal ini hanya pada kegiatan mengubah struktur function Clock menjadi class. Jangan lupa menambahkan constructor di dalam class, dan ubah function di dalam Clock menjadi method pada class.

// function Clock({ template }) {
  
//   var timer;

//   function render() {
//     var date = new Date();

//     var hours = date.getHours();
//     if (hours < 10) hours = '0' + hours;

//     var mins = date.getMinutes();
//     if (mins < 10) mins = '0' + mins;

//     var secs = date.getSeconds();
//     if (secs < 10) secs = '0' + secs;

//     var output = template
//       .replace('h', hours)
//       .replace('m', mins)
//       .replace('s', secs);

//     console.log(output);
//   }

//   this.stop = function() {
//     clearInterval(timer);
//   };

//   this.start = function() {
//     render();
//     timer = setInterval(render, 1000);
//   };

// }

// var clock = new Clock({template: 'h:m:s'});
// clock.start(); 

// function di atas diubah menjadi struktur class seperti berikut:

// class Clock {
//     // Code di sini
// }

// var clock = new Clock({template: 'h:m:s'});
// clock.start();  
console.log ("Jawaban No.5")
class Clock {
    constructor({ template }) {
        this.template = template;
    }

    render() {
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    }
    }
    var clock = new Clock({template: 'h:m:s'});
    clock.start(); 