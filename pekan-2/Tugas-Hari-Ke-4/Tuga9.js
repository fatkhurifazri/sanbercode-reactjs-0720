// return dalam fungsi di bawah ini masih menggunakan object literal
//  dalam ES5, ubahlah menjadi bentuk yang lebih sederhana di ES6.
console.log("Jawaban no.1")
const newFunction = function(firstName, lastName){
    return {
    firstName, lastName,
    fullName(){
        console.log(firstName + " " + lastName)
        return 
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()   

// Diberikan sebuah objek sebagai berikut:

// const newObject = {
//   firstName: "Harry",
//   lastName: "Potter Holt",
//   destination: "Hogwarts React Conf",
//   occupation: "Deve-wizard Avocado",
//   spell: "Vimulus Renderus!!!"
// }
// dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;
// Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)

// // Driver code
// console.log(firstName, lastName, destination, occupation)    
console.log("Jawaban No.2")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
// Driver code
const {firstName, lastName, destination, occupation}= newObject
console.log(destination)


// Kombinasikan dua array berikut menggunakan array spreading ES6

// const west = ["Will", "Chris", "Sam", "Holly"]
// const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
// //Driver Code
// console.log(combined)
console.log("Jawaban No.3")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)