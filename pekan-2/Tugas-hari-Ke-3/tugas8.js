// Soal 1

// buatlah fungsi menggunakan arrow function luas lingkaran dan keliling lingkaran dengan arrow function lalu gunakan let dan const di dalam soal ini
let luasLingkaran = (r) => {
    return 3.14 * r * r;
}
console.log(luasLingkaran(12));

const kelilingLingkaran = (r) => {
    return 2 * 3.14 * r;
}
console.log(kelilingLingkaran(10));


// Soal 2

// buatlah variable seperti di bawah ini:

// let kalimat = ""

// buatlah fungsi menambahkan kata di kalimat dan gunakan penambahan kata tersebut dengan template literal, 
// berikut ini kata kata yang mesti di tambahkan

//     saya
//     adalah
//     seorang
//     frontend
//     developer
let kalimat =""
    const kata1 = 'Saya'
    const kata2 = 'adalah'
    const kata3 = 'seorang'
    const kata4 = 'frontend'
    const kata5 = 'developer'

    let tambahKata= () => {
        
        return kalimat = `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`
    }
    console.log(tambahKata())

// soal 3

// buatlah class Book yang didalamnya terdapat property name,
// totalPage, price. lalu buat class baru komik yang extends terhadap buku dan mempunyai property sendiri yaitu isColorful yang isinya true atau false

class Book {
    constructor(name, totalPage, price) {
    this.nama = name;
    this.totalPage = totalPage ;
    this.price = price ;
    }
    get nama1() {
        return this.nama;
    }
    get totalPage1() {
        return this.totalPage;
    }
    get price1() {
        return this.price;
    }
    present() {
    return 'nama buku ' + this.nama + ", total halaman " + " " + this.totalPage + " " +", harga "+ this.price + " " ;
    }
}

class komik extends Book {
    constructor(name, totalPage, price, isColorfull) {
    super(name);
    this.nama = name;
    this.totalPage = totalPage ;
    this.price = price ;
    this.isColorfull = false;
    }
    show() {
    return this.present() + ", colorful?" + this.isColorfull;
    }
}

buku = new komik("Shincan", 20 , 15000);
console.log(buku.show());