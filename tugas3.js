/*Soal 1*/
// buatlah variabel-variabel seperti di bawah ini

// var kataPertama = "saya";
// var kataKedua = "senang";
// var kataKetiga = "belajar";
// var kataKeempat = "javascript";

// gabungkan variabel-variabel tersebut agar menghasilkan output

// saya Senang belajar JAVASCRIPT 

/*jawaban soal 1*/ 
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";
var edit = kataKedua.substr(0,1);
var besar = edit.toUpperCase();
var edit2 = kataKedua.substr(1,5);
var upper = kataKeempat.toUpperCase();
console.log(kataPertama + " " + besar + edit2 + " " + kataKetiga +" " + upper);

/*Soal 2*/ 
// buatlah variabel-variabel seperti di bawah ini

// var kataPertama = "1";
// var kataKedua = "2";
// var kataKetiga = "4";
// var kataKeempat = "5";

// ubah lah variabel diatas ke dalam integer dan lakukan jumlahkan semua variabel dan tampilkan dalam output

/*jawaban soal 2*/ 
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";
hasil= Number(kataPertama) + Number(kataKedua) + Number(kataKetiga) + Number(kataKeempat);
console.log(hasil);

/*Soal 3*/ 
// buatlah variabel-variabel seperti di bawah ini

// var kalimat = 'wah javascript itu keren sekali'; 

// var kataPertama = kalimat.substring(0, 3); 
// var kataKedua; // do your own! 
// var kataKetiga; // do your own! 
// var kataKeempat; // do your own! 
// var kataKelima; // do your own! 

// console.log('Kata Pertama: ' + kataPertama); 
// console.log('Kata Kedua: ' + kataKedua); 
// console.log('Kata Ketiga: ' + kataKetiga); 
// console.log('Kata Keempat: ' + kataKeempat); 
// console.log('Kata Kelima: ' + kataKelima);

// selesaikan variabel yang belum diisi dan hasilkan output seperti berikut:

// Kata Pertama: wah
// Kata Kedua: javascript
// Kata Ketiga: itu
// Kata Keempat: keren
// Kata Kelima: sekali

/*jawaban soal 3*/ 
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); ;
var kataKetiga = kalimat.substring(15, 19); ;
var kataKeempat = kalimat.substring(19, 25); ;
var kataKelima = kalimat.substring(25); ;

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

/*Soal 4*/ 
// buatlah variabel seperti di bawah ini

// var nilai;

// pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi

// nilai >= 80 indeksnya A
// nilai >= 70 dan nilai < 80 indeksnya B
// nilai >= 60 dan nilai < 70 indeksnya c
// nilai >= 50 dan nilai < 60 indeksnya D
// nilai < 50 indeksnya E
/*jawaban soal 4*/ 
var nilai = 25;
if ( nilai >= 80 && nilai <100 ) {
        console.log("A");
    } else if ( nilai>= 70 && nilai < 80) {
        console.log("B");
    } else if ( nilai>= 60 && nilai < 70) {
        console.log("C")
    } else if ( nilai>= 50 && nilai < 60) {
        console.log("D")
    } else{
        console.log("E")
    }

/*Soal 5*/
// buatlah variabel seperti di bawah ini

// var tanggal = 22;
// var bulan = 7;
// var tahun = 2020;

// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan,lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing-masing) 
/*jawaban soal 5*/ 
var tanggal = 5;
var bulan = 4;
var tahun = 1994;

switch(bulan){
    case 1:
        bulan="Januari";
        break;
    case 2:
        bulan="February";
        break;
    case 3:
        bulan="Maret"
        break;
    case 4:
        bulan="April";
        break;
    case 5:
        bulan="Mei";
        break;
    case 6:
        bulan="Juni";
        break;
    case 7:
        bulan="Juli";
        break;
    case 8:
        bulan="Agustus";
        break;
    case 9:
        bulan="September"
        break;
    case 10:
        bulan="Oktober";
        break;
    case 11:
        bulan="November";
        break;
    case 12:
        bulan="Desember";
        break;
    
}console.log(tanggal + " " + bulan + " " + tahun);

