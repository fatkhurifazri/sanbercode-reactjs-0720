// soal No.1
// Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript dengan menggunakan syntax while. Untuk membuat tantangan ini lebih menarik, kamu juga diminta untuk membuat suatu looping yang menghitung maju dan menghitung mundur. Jangan lupa tampilkan di console juga judul ‘LOOPING PERTAMA’ dan ‘LOOPING KEDUA’.”

// Output:

// LOOPING PERTAMA
// 2 - I love coding
// 4 - I love coding
// 6 - I love coding
// 8 - I love coding
// 10 - I love coding
// 12 - I love coding
// 14 - I love coding
// 16 - I love coding
// 18 - I love coding
// 20 - I love coding
// LOOPING KEDUA
// 20 - I will become a frontend developer
// 18 - I will become a frontend developer                                                                              
// 16 - I will become a frontend developer
// 14 - I will become a frontend developer
// 12 - I will become a frontend developer
// 10 - I will become a frontend developer
// 8 - I will become a frontend developer
// 6 - I will become a frontend developer
// 4 - I will become a frontend developer
// 2 - I will become a frontend developer
// console.log("LOOPING PERTAMA");
console.log("jawaban no.1")

var x = 2;
var y = "I love coding"
while (x !==22) {
  if (x % 2 == 0)
    console.log(x + " " + "-" + " " + y);
  x++;
}

console.log("LOOPING KEDUA")

var x=20;
var y = "I love coding";
while (x !==2) {
    if (x % 2 == 0)
    console.log(x + " " + "-" + " " + y);
    x--;
}

// soal No.2
// Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript dengan menggunakan syntax for. 
// Untuk membuat tantangan ini lebih menarik, kamu juga diminta untuk memenuhi syarat tertentu yaitu:

// SYARAT:
// A. Jika angka ganjil maka tampilkan Santai
// B. Jika angka genap maka tampilkan Berkualitas
// C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding.

// Output:

// 1 - Santai
// 2 - Berkualitas
// 3 - I Love Coding 
// 4 - Berkualitas
// 5 - Santai
// 6 - Berkualitas
// 7 - Santai
// 8 - Berkualitas
// 9 - I Love Coding
// 10 - Berkualitas
// 11 - Santai
// 12 - Berkualitas
// 13 - Santai
// 14 - Berkualitas
// 15 - I Love Coding
// 16 - Berkualitas
// 17 - Santai
// 18 - Berkualitas
// 19 - Santai
// 20 - Berkualitas
console.log("Jawaban No.2")
for (var x=1; x<=20; x++) {
    if (x%3 ==0 && x%2 ==1){
    console.log( x + " - I Love coding");
  }else if (x%2 ==0){
    console.log( x + " - Berkualitas");
  }else if(x%2 !==0){
    console.log( x + " - Santai")
  }
}

// soal No.3
// Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi 7 dan alas 7. Looping boleh menggunakan syntax apa pun (while, for, do while).

// Output:

// #
// ##
// ###
// ####
// #####
// ######
// #######
console.log("Jawaban No.3")
for (var i = 7; i >= 1; i--) {
  var a = "";
  for (var j = i; j <= 7; j++) {
  a += "*";
     }
 console.log(a);
}

// buatlah variabel seperti di bawah ini

// var kalimat="saya sangat senang belajar javascript"

// ubah kalimat diatas menjadi array dengan output seperti di bawah ini

// ["saya", "sangat", "senang", "belajar", "javascript"]

console.log("jawaban No.4")
var kalimat="saya sangat senang belajar javascript"
var pecah=kalimat.split(" ");
console.log(pecah);

// buatlah variabel seperti di bawah ini

// var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];

// urutkan array di atas dan tampilkan data seperti output di bawah ini:

// 1. Mangga
// 2. Apel
// 3. Anggur
// 4. Semangka
// 5. Jeruk

console.log("Jawaban No.5")
var daftarBuah =["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();
for( i = 0; i<=4; i++){
  console.log(daftarBuah[i]);
}